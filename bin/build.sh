#!/usr/bin/env bash

set -e

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
etc_dir=${script_dir}/../etc

mkdir -p ${dist_dir}

files=$(cd ${src_dir} && find . -not -path '*/\.*' -type f | sort)

for file in ${files}; do
  basename=$(basename -- "${file}")
  dirname=$(dirname -- "${file}")
  filename="${basename%.*}"
  sfile=${src_dir}/${file}

  echo ${filename}
  copy_command="rsync -aH ${sfile} ${dist_dir}/${dirname}/"
  mkdir -p ${dist_dir}/${dirname}/
  `${copy_command}`

  if [[ $file == *.md ]] || [[ $file == *.markdown ]]; then
    for format in pdf html docx fodt; do
      echo "  markdown->${format}"
      pandoc -M mainfont="DejaVu Sans Mono" --pdf-engine=xelatex ${sfile} -o ${dist_dir}/${dirname}/${filename}.${format}
    done
  else if [[ $file == *.fodt ]] || [[ $file == *.odt ]]; then  
    for format in pdf html docx; do
      echo "  libreoffice->${format}"
      libreoffice --invisible --headless --convert-to ${format} ${sfile} --outdir ${dist_dir}/${dirname}
    done
  else if [[ $file == *.jpg ]] || [[ $file == *.jpeg ]]; then
    for format in png svg; do
      echo "  jpeg->${format}"
      convert ${sfile} ${dist_dir}/${dirname}/${filename}.${format}
    done
  else if [[ $file == *.png ]]; then
    for format in jpg svg; do
      echo "  png->${format}"
      convert ${sfile} ${dist_dir}/${dirname}/${filename}.${format}
    done
  else if [[ $file == *.svg ]]; then
    for format in jpg png; do
      echo "  svg->${format}"
      convert ${sfile} ${dist_dir}/${dirname}/${filename}.${format}
    done
  fi
  fi
  fi
  fi
  fi
done

echo "copy ${etc_dir}"
rsync -aH ${etc_dir}/ ${dist_dir}
